import Vue from 'vue'
import Router from 'vue-router'
import Post from '@/components/Post'
import PostCreate from '@/components/PostCreate'
import PostEdit from '@/components/PostEdit'
import PostDetail from '@/components/PostDetail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Post',
      component: Post
    },
    {
      path: '/PostCreate',
      name: 'PostCreate',
      component: PostCreate
    },
    {
      path: '/PostEdit/:id',
      name: 'PostEdit',
      component: PostEdit,
      props: true
    },
    {
      path: '/PostDetail/:id',
      name: 'PostDetail',
      component: PostDetail,
      props: true
    }
  ]
})
